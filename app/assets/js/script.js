let isMenuClose = true;
let isSearchClose = true;

function onMenuToggle() {
    const menu = document.getElementById('menu');
    if (isMenuClose) {
        menu.classList.add('active');
        isMenuClose = false;
    } else {
        menu.classList.remove('active');
        isMenuClose = true;
    }
}
function onSearchToggle() {
    const nav = document.getElementById('serach-box');

    if (isSearchClose) {
        nav.classList.add('active');
        isSearchClose = false;
    } else {
        nav.classList.remove('active');
        isSearchClose = true;
    }
}